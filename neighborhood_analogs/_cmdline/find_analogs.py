from neighborhood_analogs import img_scale
from PIL import Image, ImageEnhance
from pyFIT3D.common.constants import __c__
from pyFIT3D.common.stats import shift_convolve
from pyFIT3D.modelling.dust import spec_apply_dust
from pyFIT3D.common.io import get_wave_from_header, array_to_fits
from astropy import units as u
from astropy.constants import L_sun
import warnings
import sys
import os
import argparse
import itertools as it
from pprint import pprint
from copy import deepcopy as copy
from tqdm import tqdm
import numpy as np
from scipy.interpolate import griddata
from scipy.ndimage import generic_filter, gaussian_filter
import pandas as pd
from astropy.io import fits
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
import seaborn as sns

sns.set_theme(context="poster", style="ticks", palette="Set2", color_codes=True)


warnings.filterwarnings('ignore', category=UserWarning)

from MaStarChecks.plotting.styles import *

from neighborhood_analogs.constants import CALIFA_CATALOG_URLS, MASTAR_DATA_PATH
from neighborhood_analogs.constants import CALIFA_SCALE, CALIFA_PSF, CALIFA_INST, CALIFA_ALPHA
from neighborhood_analogs.constants import NORM_WINDOW, FITTING_WINDOW, CHISQ_WINDOW, RGB_WAVELENGTHS, RGB_HW
from neighborhood_analogs.constants import HUBBLE_CLASSIFICATION
from neighborhood_analogs.constants import OBS_CUBE_FMT, MOD_CUBE_FMT, SPS_CUBE_FMT
from neighborhood_analogs.constants import SFRG_MAP_FMT, SFRS_MAP_FMT, MLR_MAP_FMT, SMAS_MAP_FMT, SPS_MAP_NAMES
from neighborhood_analogs.constants import SNA_MAP_IXLOG, SNA_MAP_NAMES

CWD = os.path.abspath(os.path.curdir)


def _no_traceback(type, value, traceback):
    print(value)


def weighted_mean_sdev(samples, weights):
    mask = weights != 0.0
    M = mask.sum()

    mean = np.sum(weights * samples, axis=-1) / np.sum(weights, axis=-1)
    sdev = np.sqrt(np.sum(weights*(samples - mean)**2) /
                   (((M-1)/M) * np.sum(weights)))
    return mean, sdev


def interpolate_map(map_, invalid_value=9999):
    # if no valid data to interpolate from, return original map
    if (np.isnan(map_)|np.isinf(map_)).all(): return map_
    # TODO: if no invalid points to interpolate, return original map

    map_[np.isnan(map_)|np.isinf(map_)] = invalid_value

    yo, xo = np.where(map_!=invalid_value)
    values = map_[yo,xo]

    yi, xi = np.where(map_>-invalid_value)

    return griddata(
        np.column_stack((xo,yo)),
        values,
        np.column_stack((xi,yi)),
        method="nearest"
    ).reshape(map_.shape)


def read_map(filename):
    map_ = fits.getdata(filename)
    return interpolate_map(map_)


def load_mastar_data(path, return_spectra=False, return_catalogue=False, verbose=True):
    mastar = fits.open(path)
    sna_wl = get_wave_from_header(mastar["SPECTRA"].header)
    mastar_sed = mastar["SPECTRA"].data
    # compute SN spectrum
    Lsun_per_angstroms = mastar["CATALOGUE"].data.FNORM[:, None] * (np.pi*(mastar["CATALOGUE"].data.DIST[:, None]*u.kpc).to(
        "cm") / L_sun.cgs) * np.pi*(mastar["CATALOGUE"].data.DIST[:, None]*u.kpc).to("cm")
    sna_sed = (mastar_sed * Lsun_per_angstroms *
               mastar["CATALOGUE"].data.VCORR[:, None]).sum(axis=0).value
    # compute the FWHM radius enclosing the solar neighborhood (SN)
    _, sigma_dist = weighted_mean_sdev(
        mastar["CATALOGUE"].data.DIST, mastar["CATALOGUE"].data.VCORR)
    if verbose:
        tqdm.write(f"\n{sigma_dist = :.2f} kpc")

    returns = sna_wl, sna_sed, sigma_dist
    if return_spectra:
        returns += (mastar["SPECTRA"].data,)
    if return_catalogue:
        returns += (mastar["CATALOGUE"].data,)
    return returns


def load_catalogues(catalogue_path, morph_path, agn_path, vcor_path, hubble_map=None, verbose=True):
    with open(catalogue_path) as f:
        nheader = 0
        line = f.readline()[:-1]
        while not line.startswith("# HEADER"):
            nheader += 1
            line = f.readline()[:-1]
        names = np.asarray(line.strip("# HEADER |").split())
    _, unique_idx = np.unique(names, return_index=True)
    names[unique_idx] = [f"{name}_1" for name in names[unique_idx]]
    califa_catalogue = pd.read_csv(catalogue_path, skiprows=nheader, comment="#", names=names)
        
    idx_cols = [0, 14, 72, 73, 126, 127, 128, 130, 131, 132, 167, 168]
    nam_cols = ["name", "redshift", "mass_star", "sfr_star", "re_arc", "re_kpc", "dl", "pa", "ellipticity", "inclination", "xc", "yc"]
    califa_catalogue = califa_catalogue.rename(columns=dict(zip(
        califa_catalogue.columns[idx_cols],
        nam_cols
    )))
    califa_catalogue.name = califa_catalogue.name.str.strip()
    califa_catalogue.set_index("name", inplace=True)
    # fixing position angle to run in the range [0, 180)
    califa_catalogue.pa = califa_catalogue.pa.apply(lambda theta: theta if theta<180-1e-12 else theta-180)

    # read morphological catalogue
    califa_morph = pd.read_csv(
        morph_path, comment="#",
        usecols=(2, 5, 6, 7),
        names=["name", "type", "bar", "merger"]
    )
    califa_morph.name = califa_morph.name.str.strip()
    califa_morph.set_index("name", inplace=True)
    califa_morph.dropna(how="any", axis="index", inplace=True)
    califa_morph.type = califa_morph.type.apply(lambda v: hubble_map[int(v)])
    califa_morph.bar = califa_morph.bar.astype(bool)
    califa_morph.merger = califa_morph.merger.astype(bool)

    # read AGN catalogue
    califa_agn = pd.read_csv(agn_path)
    califa_agn = califa_agn.rename(
        columns={"DBName": "name", "AGN_FLAG": "agn"})
    califa_agn = califa_agn.filter(items=("name", "agn"))
    califa_agn.name = califa_agn.name.str.strip()
    califa_agn.set_index("name", inplace=True)
    califa_agn.agn = califa_agn.agn > 0

    # read volume correction catalogue
    califa_vcor = pd.read_csv(vcor_path, comment="#", usecols=(0,43,63), names=["name", "C", "vcor"])
    califa_vcor.name = califa_vcor.name.str.strip()
    califa_vcor.set_index("name", inplace=True)
    # califa_vcor.vcor /= califa_vcor.vcor.sum()

    # build master table
    califa_table = pd.concat((califa_catalogue, califa_morph, califa_agn, califa_vcor), join="inner", axis="columns")
    if verbose:
        tqdm.write(f"# rows catalogue        : {califa_catalogue.shape[0]}")
        tqdm.write(f"# rows morphology       : {califa_morph.shape[0]}")
        tqdm.write(f"# rows AGN              : {califa_agn.shape[0]}")
        tqdm.write(f"# rows volume correction: {califa_vcor.shape[0]}")
        tqdm.write(f"                   total: {califa_table.shape[0]}\n")
        tqdm.write(f"final catalogue (first {10} rows):\n")
        tqdm.write(califa_table.round(4).head(10).to_string())

    return califa_table


def filter_general(catalogue, verbose=True):
    mask_good_re = catalogue.inclination != 0
    mask_faceon = catalogue.inclination <= 60
    filtered_catalogue = catalogue.loc[mask_good_re & mask_faceon].sort_values(
        by="inclination")

    if verbose:
        tqdm.write(
            f"\ngalaxies {len(filtered_catalogue)}/{len(catalogue)}")
        tqdm.write("catalogue:\n")
        tqdm.write(filtered_catalogue.round(4).to_string())

    return filtered_catalogue


def filter_mwlike(catalogue, verbose=True):
    mask_good_re = catalogue.inclination != 0
    mask_faceon = catalogue.inclination <= 60
    mask_morph = catalogue.type.isin("Sb Sbc".split())
    # mask_morph = catalogue.type.isin([f"E{i}" for i in range(8)]+["S0","S0a"])
    mask_mass = (catalogue.mass_star >= 10.5) & (catalogue.mass_star <= 11)
    # mask_sfr = (catalogue.sfr_star >= 0.05) & (catalogue.sfr_star <= 0.35)
    mask_merger = ~catalogue.merger
    mask_agn = ~catalogue.agn
    filtered_catalogue = catalogue.loc[mask_good_re & mask_mass & mask_faceon &
                                       mask_morph & mask_merger & mask_agn].sort_values(by="inclination")

    if verbose:
        tqdm.write(
            f"\nMW-like galaxies {len(filtered_catalogue)}/{len(catalogue)}")
        tqdm.write("catalogue:\n")
        tqdm.write(filtered_catalogue.round(4).to_string())

    return filtered_catalogue


def create_RGB(cube, wavelength, R_filt, G_filt, B_filt):
    """
    return RGB image object from a given cube and filters

    Parameters:
    -----------
    cube: 3-array_like
        cube to compute RGB image from
    {R,G,B}_filt: list_like
        a two-element object containing the range in wavelength of each filter

    Returns:
    --------
    RGB_image: image.Image object
        the RGB composite image    
    """

    Rdata = np.mean(cube[(R_filt[0] <= wavelength) & (
        wavelength <= R_filt[1])], axis=0) * 1.2
    Gdata = np.mean(cube[(G_filt[0] <= wavelength) & (
        wavelength <= G_filt[1])], axis=0) * 0.9
    Bdata = np.mean(cube[(B_filt[0] <= wavelength) & (
        wavelength <= B_filt[1])], axis=0) * 1.1
    
    scale_flux = 0.7  # 2.5
    RGBdata = np.zeros((Bdata.shape[0], Bdata.shape[1], 3), dtype=float)
    RGBdata[:, :, 0] = img_scale.sqrt(
        Rdata*scale_flux, scale_min=0.01, scale_max=2)
    RGBdata[:, :, 1] = img_scale.sqrt(
        Gdata*scale_flux, scale_min=0.01, scale_max=2)
    RGBdata[:, :, 2] = img_scale.sqrt(
        Bdata*scale_flux, scale_min=0.01, scale_max=2)

    RGBdata = RGBdata * 255
    RGBdata_int = RGBdata.astype('uint8')

    RGB_image = Image.fromarray(RGBdata_int)

    bright = ImageEnhance.Brightness(RGB_image)
    RGB_image = bright.enhance(1.2)
    contrast = ImageEnhance.Contrast(RGB_image)
    RGB_image = contrast.enhance(1.5)
    sharpness = ImageEnhance.Sharpness(RGB_image)
    RGB_image = sharpness.enhance(2.0)

    return RGB_image


def _main(cmd_args=sys.argv[1:]):
    parser = argparse.ArgumentParser(
        description="Find solar neighborhood analogs in other galaxies analysed with Pipe3D"
    )
    parser.add_argument(
        "dp_path", metavar="dp-path",
        help="the path where to find the Pipe3D data products"
    )
    parser.add_argument(
        "--dp-kind", nargs="+",
        help="the path structure the data products. Valid values are 'califa' and 'manga'",
        default="califa"
    )
    parser.add_argument(
        "--catalogs", nargs=3, metavar=("morphology", "allgood", "AGN"),
        help=f"morphology, all-good & AGN catalogs. Defaults to: {CALIFA_CATALOG_URLS}",
        default=CALIFA_CATALOG_URLS
    )
    parser.add_argument(
        "--mastar-catalog",
        help=f"the mastar catalog as constructed by CoSHA. Defaults to {MASTAR_DATA_PATH}",
        default=MASTAR_DATA_PATH
    )
    parser.add_argument(
        "--no-filter", action="store_true",
        help=f"whether to filter the whole sample by MW-like galaxies or not. Defaults to filter"
    )
    parser.add_argument(
        "--pixel-scale", type=float,
        help=f"the scale of the pixel in the spatial dimensions in arcsec/pix. Defaults to {CALIFA_SCALE}",
        default=CALIFA_SCALE
    )
    parser.add_argument(
        "--psf", type=float,
        help=f"the instrumental PSF (FWHM) in the spatial dimensions in arcsec. Defaults to {CALIFA_PSF}",
        default=CALIFA_PSF
    )
    parser.add_argument(
        "--spectral-resolution", type=float,
        help=f"spectral resolution (FWHM) of the survey in angstroms. Defaults to {CALIFA_INST}",
        default=CALIFA_INST
    )
    parser.add_argument(
        "--alpha", type=float,
        help=f"factor for spatial covariance correction in spectral error propagation. Defaults to {CALIFA_ALPHA}",
        default=CALIFA_ALPHA
    )
    parser.add_argument(
        "--norm-window", nargs=2, type=float,
        help=f"normalization window in wavelength space (angstroms). Defaults to {NORM_WINDOW}",
        default=NORM_WINDOW
    )
    parser.add_argument(
        "--fitting-window", nargs=2, type=float,
        help=f"fitting wavelength window (angstroms). Defaults to {FITTING_WINDOW}",
        default=FITTING_WINDOW
    )
    parser.add_argument(
        "--chisq-penalty", nargs=2, type=float,
        help=f"chi square range where a spaxel is considered a likely SNA. Defaults to {CHISQ_WINDOW}",
        default=CHISQ_WINDOW
    )
    parser.add_argument(
        "--rgb-bands", nargs=3, type=float,
        help=f"central wavelength passbands for RGB image (angstroms). defaults to {RGB_WAVELENGTHS}",
        default=RGB_WAVELENGTHS
    )
    parser.add_argument(
        "--rgb-hw", type=float,
        help=f"half width for the RGB passbands. Defaults to {RGB_HW}",
        default=RGB_HW
    )
    parser.add_argument(
        "-o", "--output-path", metavar="path",
        help=f"path to the outputs. Defaults to '{CWD}'",
        default=CWD
    )
    parser.add_argument(
        "--rewrite", action="store_true",
        help=f"whether to rewrite past runs or not. Defaults to skip past runs"
    )
    parser.add_argument(
        "-v", "--verbose",
        help="if given, shows information about the progress of the script. Defaults to false.",
        action="store_true"
    )
    parser.add_argument(
        "-d", "--debug",
        help="debugging mode. Defaults to false.",
        action="store_true"
    )
    args = parser.parse_args(cmd_args)
    if not args.debug:
        sys.excepthook = _no_traceback
    else:
        pprint("COMMAND LINE ARGUMENTS")
        pprint(f"{args}\n")

    # convert psf to pixel units
    args.psf = args.psf / 2.355 / args.pixel_scale
    # convert instrumental resolution to differential resolution in angstroms
    sigma_inst = np.sqrt((args.spectral_resolution/2.355)**2-(2.6/2.355)**2)
    # define RGB windows
    b_window = (args.rgb_bands[0]-args.rgb_hw, args.rgb_bands[0]+args.rgb_hw)
    g_window = (args.rgb_bands[1]-args.rgb_hw, args.rgb_bands[1]+args.rgb_hw)
    r_window = (args.rgb_bands[2]-args.rgb_hw, args.rgb_bands[2]+args.rgb_hw)

    # read MaStar data
    sna_wl, sna_sed, sigma_dist = load_mastar_data(
        path=args.mastar_catalog, verbose=args.verbose)
    # read catalogue
    califa_table = load_catalogues(
        catalogue_path=args.catalogs[1],
        morph_path=args.catalogs[0],
        agn_path=args.catalogs[2],
        vcor_path=args.catalogs[3],
        hubble_map=HUBBLE_CLASSIFICATION,
        verbose=args.verbose
    )
    # general quality filters
    califa_table = filter_general(catalogue=califa_table, verbose=args.verbose)
    # filter MW-like catalogue
    if not args.no_filter:
        califa_table = filter_mwlike(catalogue=califa_table, verbose=args.verbose)

    tqdm.write("\ncomputing SNA likelihood:\n")
    # TODO: paralellize this cycle
    # califa_table = califa_table.filter(items=["NGC5947"], axis="index")
    for name, row in tqdm(califa_table.iterrows(), total=len(califa_table), ascii=True, unit="galaxy"):
        # define image name
        image_path = f"{args.output_path}/{name}-sna-likelihood.png"
        # check if is already done
        if os.path.isfile(image_path) and not args.rewrite: continue

        # compute galaxy physical scale
        galaxy_scale = row.re_arc / row.re_kpc
        # compute voxel radius using galaxy scale
        voxel_radius = int(np.floor(sigma_dist * galaxy_scale / args.pixel_scale))
        # tqdm.write(f"\nfor galaxy '{name}', {galaxy_scale = :.2f} ''/kpc, {voxel_radius = } pixels =======================")

        # read original (observed), stellar model & SPS cube
        obs = fits.open(os.path.join(args.dp_path, OBS_CUBE_FMT.format(name)))
        mod = fits.open(os.path.join(args.dp_path, MOD_CUBE_FMT.format(name)))
        sps = fits.open(os.path.join(args.dp_path, SPS_CUBE_FMT.format(name)))
        # read additional physical properties maps
        sfg_map = read_map(os.path.join(args.dp_path, SFRG_MAP_FMT.format(name)))
        sfs_map = read_map(os.path.join(args.dp_path, SFRS_MAP_FMT.format(name)))
        mas_map = read_map(os.path.join(args.dp_path, SMAS_MAP_FMT.format(name)))
        mlr_map = read_map(os.path.join(args.dp_path, MLR_MAP_FMT.format(name)))
        # mlr_map = mas_map - interpolate_map(np.log10(
        #     sps["SSP"].data[0] * 1e-16 * 4 * np.pi * (row.dl / 3.828e+33) * row.dl / (4 * np.pi * (args.pixel_scale / galaxy_scale)**2),
        #     where=sps["SSP"].data[0]>0,
        #     out=np.full_like(sps["SSP"].data[0], np.nan, dtype=np.double)
        # ))
        add_map = [
            sfg_map,
            sfs_map,
            mas_map,
            mlr_map
        ]

        # compute window given the voxel radius
        window = np.ones((2*voxel_radius+1, 2*voxel_radius+1))
        pixels = np.arange(2*voxel_radius+1)
        I, J = np.meshgrid(pixels, pixels)
        circle_mask = np.sqrt((I-voxel_radius)**2 +
                              (J-voxel_radius)**2) > voxel_radius
        window[circle_mask] = 0

        # compute moving summation of the spectra within the given window
        mod_cube = mod[0].data
        # compute bad pixels mask
        badpix_mask = ((obs["BADPIX"].data) | (
            obs[0].data <= 0)).astype(bool).all(axis=0)
        # make NaN all bad pixels
        mod_cube[:, badpix_mask] = np.nan
        # tqdm.write(f"\nresampling spectra cube")

        mod_cube_rs = generic_filter(
            mod_cube, function=np.sum, footprint=window[None], mode="nearest")
        array_to_fits(f"{args.output_path}/{name}_mod.cube.filtered.fits.gz",
                      mod_cube_rs, header=mod[0].header, overwrite=True)

        # propagate errors using the same moving window
        # tqdm.write(f"\nconvolving error cube")
        err_cube = gaussian_filter(obs[1].data, sigma=(
            0, args.psf, args.psf), mode="nearest")
        err_cube = obs[1].data
        N = window.sum()
        covar = 1 + args.alpha * np.log10(N)
        err_cube[:, badpix_mask] = np.nan
        # tqdm.write(f"\npropagating errors")
        err_cube_rs = generic_filter(err_cube, function=lambda errors: np.sqrt(
            np.sum(errors**2)/N)*covar, footprint=window[None], mode="nearest")
        err_cube_rs[err_cube_rs > 1] = np.inf
        array_to_fits(f"{args.output_path}/{name}_err.cube.filtered.fits.gz",
                      err_cube_rs, header=mod[0].header, overwrite=True)

        # compute moving stat on non-linear parameters: Av, LOSVD, redshift
        # tqdm.write(f"\nresampling physical properties maps")
        par_cube = sps["SSP"].data[list(SPS_MAP_NAMES.values())]
        # append additional physical maps
        par_cube = np.concatenate([par_cube]+[map_[None] for map_ in add_map])
        par_cube[SNA_MAP_IXLOG] = 10**par_cube[SNA_MAP_IXLOG]
        par_cube_rs = generic_filter(par_cube, function=np.mean, footprint=window[None], mode="nearest")
        par_cube_rs[SNA_MAP_IXLOG] = np.log10(
            par_cube_rs[SNA_MAP_IXLOG], where=par_cube_rs[SNA_MAP_IXLOG]!=0, out=np.zeros_like(par_cube_rs[SNA_MAP_IXLOG]))
        par_cube_rs[:, badpix_mask] = np.nan
        par_header = copy(mod[0].header)
        del par_header["*3"]
        for par_name, i in SNA_MAP_NAMES.items():
            par_header[f"NAME{i}"] = tuple(par_name.split())
        array_to_fits(f"{args.output_path}/{name}_par.cube.filtered.fits.gz",
                      par_cube_rs, header=par_header, overwrite=True)

        # compute SN analog cube
        obs_wl = get_wave_from_header(obs[0].header, wave_axis=3)
        sna_cube = np.zeros_like(obs[0].data)
        # tqdm.write(f"\ncomputing SNA cube")
        for i, j in it.product(range(badpix_mask.shape[0]), range(badpix_mask.shape[1])):
            if badpix_mask[i, j]:
                continue
            sna_sed_dusty = spec_apply_dust(sna_wl, sna_sed, AV=par_cube_rs[SNA_MAP_NAMES["Av [mag]"]][i, j])
            sna_cube[:, i, j] = shift_convolve(
                obs_wl, sna_wl, sna_sed_dusty,
                redshift=par_cube_rs[SNA_MAP_NAMES["vel [km/s]"]][i, j] / __c__,
                sigma=par_cube_rs[SNA_MAP_NAMES["LOSVD [km/s]"]][i, j],
                sigma_inst=sigma_inst
            )
        sna_cube[:, badpix_mask] = np.nan
        array_to_fits(f"{args.output_path}/{name}_sna.cube.fits.gz",
                      sna_cube, header=mod[0].header, overwrite=True)

        # compute chi-square between SN spectrum and analog candidates
        slice_mask = (args.norm_window[0] <= obs_wl) & (
            obs_wl <= args.norm_window[1])
        wl_mask = (args.fitting_window[0] <= obs_wl) & (
            obs_wl <= args.fitting_window[1])

        sna_map = np.median(sna_cube[slice_mask], axis=0)
        mod_map = np.median(mod_cube_rs[slice_mask], axis=0)
        snr_map = np.median(np.divide(
            mod_cube_rs, err_cube_rs, where=err_cube_rs!=0, out=np.zeros_like(err_cube_rs)
            )[slice_mask], axis=0)
        nor_map = np.divide(mod_map, sna_map, where=sna_map !=
                            0, out=np.zeros_like(mod_map))
        # tqdm.write(f"\ncomputing chi square map")
        chi = np.divide(
            (mod_cube_rs-sna_cube*nor_map)[wl_mask],
            err_cube_rs[wl_mask],
            where=err_cube_rs[wl_mask] != 0,
            out=np.zeros_like(mod_cube_rs[wl_mask])
        )
        chi_map = np.sum(chi**2, axis=0) / (wl_mask.sum()-1)
        total_mask = badpix_mask | (chi_map < 0) | (
            snr_map <= 40) | (mod_map <= 0.)
        chi_map[total_mask] = np.nan

        # compute likelihood
        # tqdm.write(f"\ncomputing likelihood map")
        # BUG: compute gaussian normalization correctly in order to avoid over-weighting low SNR pixels
        # np.ma.masked_array(np.nanprod(1/(err_cube_rs[wl_mask]*np.sqrt(2*np.pi)), axis=0), mask=total_mask)
        gauss_sigma = 1.0
        lik_map = gauss_sigma*np.exp(-0.5*chi_map)
        if np.isnan(lik_map).all():
            lik_map = np.zeros_like(lik_map)
        else:
            lik_map = lik_map / (np.nanmax(lik_map) if np.nanmax(lik_map) != 0 else 1.0)
        lik_map[total_mask] = np.nan

        sna_hdus = fits.HDUList()
        map_cube_rs = np.dstack((
            sna_map, mod_map, snr_map, nor_map,
            np.nan_to_num(chi_map, nan=-1.0), np.nan_to_num(lik_map, nan=-1.0),
            badpix_mask.astype(float))).transpose((2, 0, 1))
        map_header = copy(mod[0].header)
        del map_header["*3"]
        map_header["NAME0"] = ("SNA", "[flux]")
        map_header["NAME1"] = ("model", "[flux]")
        map_header["NAME2"] = ("SNR", "")
        map_header["NAME3"] = ("normalization", "")
        map_header["NAME4"] = ("chi square", "")
        map_header["NAME5"] = ("likelihood", "")
        map_header["NAME6"] = ("badpix", "")
        sna_hdu1 = fits.PrimaryHDU(header=map_header, data=map_cube_rs)
        sna_hdu1.name = "MAPS"
        sna_hdus.append(sna_hdu1)

        y, x = np.where((args.chisq_penalty[0] <= chi_map) & (chi_map <= args.chisq_penalty[1]))
        if x.size != 0:
            y_sna, x_sna = np.unravel_index(np.nanargmin(np.abs(chi_map-1.0)), chi_map.shape)
        else: y_sna, x_sna = np.nan, np.nan
        sna_hdu2 = fits.BinTableHDU.from_columns([
            fits.Column(name="x", array=x, format="K"),
            fits.Column(name="y", array=y, format="K")
        ])
        sna_hdu2.name = "SNA"
        sna_hdus.append(sna_hdu2)
        sna_hdus.writeto(f"{args.output_path}/{name}_lik.cube.fits.gz", overwrite=True)

        # draw chi square, likelihood maps
        fig, axs = plt.subplots(1, 2, figsize=(
            20, 25), sharex=True, sharey=True)

        rgb_image = rgb_image = create_RGB(
            cube=obs[0].data,
            wavelength=obs_wl,
            R_filt=(6200,6700),
            G_filt=(5100,5650),
            B_filt=(3770,3900)
        )
        axs[0].imshow(rgb_image, origin="lower")
        axs[0].contour(lik_map, levels=(0.01, 0.05, 0.32), colors="w", linestyles=(":", "--", "-"), linewidths=0.7)
        axs[0].plot(x, y, "o", ms=3, mew=0, mfc="#FDC1C5")
        axs[0].plot(x_sna, y_sna, "o", ms=6, mew=1, mfc="none", mec="#FD5956")

        im = axs[1].imshow(lik_map, origin="lower", cmap="binary_r", vmin=0, vmax=1)
        axi = inset_axes(
            axs[1],
            width="25%",
            height="2%",
            loc='upper right'
        )
        axi.tick_params(labelsize="xx-small", size=3)
        cb = plt.colorbar(im, cax=axi, orientation="horizontal")
        cn = axs[1].contour(lik_map, levels=(0.01, 0.05, 0.32), colors="w", linestyles=(":", "--", "-"), linewidths=0.7)
        cb.set_label(r"$\mathcal{L}_\mathrm{SNA}$", fontsize="xx-small")

        axs[0].set_xlabel("$X$ (pixel)")
        axs[0].set_ylabel("$Y$ (pixel)")
        axs[1].set_xlabel("$X$ (pixel)")

        # fig.tight_layout()
        fig.savefig(image_path, bbox_inches="tight")
        plt.close(fig)

        # smooth maps using imaging techniques

    # solve drake's equation
    # volume correct the sample in the local universe
    return None
