
import numpy as np
import pandas as pd
from scipy.optimize import least_squares

from neighborhood_analogs.constants import FIELD_LATEX, R_SUN, RE_MW
from neighborhood_analogs.models import Sigma_mass


TABLE_MW = pd.DataFrame(index=FIELD_LATEX, columns=["mean", "std"], data=[
    # Bland-Hawthorn+2016
    # r"S(B)b~--~c",
    [13, 1],
    # Bland-Hawthorn+2016
    [np.log10(5e10), np.abs(1e10 / 5e10 / np.log(10))],
    # Bland-Hawthorn+2016, Fraser-McKelvie+2019
    [np.log10(1.65), np.abs(0.19 / 1.65 / np.log(10))],
    # Bland-Hawthorn+2016 (radial scale 2.5kpc)
    [RE_MW, 0.4],
    # Gontcharov+2021 (red clump stars, <1.7kpc)
    [np.log10(2.3e9 + 3.2e9*0.025), np.abs(np.sqrt(3.2e9**2*0.005**2 + \
                                                   0.5e9**2 + 0.025**2*1.6e9**2) / (2.3e9 + 3.2e9*0.025) / np.log(10))],
    # Reid+2007 (dwarfs, <30pc)
    [np.log10(5e9), np.abs(2.5e9 / 5e9 / np.log(10))],
    # Binney+2000
    # [np.log10(11.2e9), np.abs(0.75e9 / 11.2e9 / np.log(10))],
    # Gontcharov+2021 (red clump stars, <1.7kpc)
    [-0.08 - 0.16*0.0, np.sqrt(0.16**2 + 0.08**2 + 0.0**2*0.07**2)],
    # Ding+2019 (dwarfs, <1kpc), Hayden+2020 (mostly dwarfs, <0.5kpc); Rebasa-Mansergas+2021 (dwarf binaries, <0.5kpc)
    [-0.06, 0.2],
    # Gontcharov+2021 (red clump stars, <1.7kpc); Rebasa-Mansergas+2021 (dwarf binaries, <0.5kpc); Fitzpatrick+2019
    [0.06*3.1, 0.02],
    # Flynn+2006
    # [np.log10(1.5/0.66), np.abs(0.2 / (1.5/0.66) / np.log(10))],
    [np.log10(1.5/0.66), np.abs(0.2 / (1.5/0.66) / np.log(10))],
    # Hagen & Helmi 2017 (in the z direction); Nesti & Salucci 2013 (model for the thin disk surface density)
    [np.log10(Sigma_mass(R_SUN*RE_MW)/1000**2), np.abs(np.sqrt((1/(2*np.pi*2.5**2*1000**2)*np.exp(-8.2/2.5))**2*1e10**2 + (1e10/(np.pi*2.5**4*1000**4)*(8.2*1000/2-2.5*1000)
                                                                                                                           * np.exp(-8.2/2.5))**2*0.4**2*1000**2 + (1e10/(2*np.pi*2.5**3*1000**3)*np.exp(-8.2/2.5))**2*0.1**2*1000**2)/(Sigma_mass(R_SUN*RE_MW)/1000**2)/np.log(10))],
    # Bonatto & Bica 2011 (stellar clusters <16per cent of total SFR, <1kpc)
    [np.log10((1+100/16)*790/1e6/1000**2), np.abs((1+100/16)*160 / \
                                                  1e6/1000**2 / ((1+100/16)*790/1e6/1000**2) / np.log(10))],
    # Blanc-Hawthorn+2016
    [R_SUN, R_SUN * np.sqrt((0.1/(8.2))**2 + (0.4/RE_MW)**2)],
    # TODO: missing systemic velocity in the SN
    [np.nan, np.nan],
    # TODO: missing LOSVD in the SN
    [np.nan, np.nan],
    # TODO: missing velocity to LOSVD ratio in the SN
    [np.nan, np.nan]
]).T


def format_name(name):
    name_chars = np.asarray(list(name))
    alpha_mask = np.asarray(list(map(str.isalpha, name_chars)))
    cat_name = "".join(name_chars[alpha_mask])
    idx_name = "".join(name_chars[~alpha_mask]).lstrip("0")
    fmt_name = fr"{cat_name}$~{idx_name}$"
    return fmt_name


def categorical_mode_sdev(counts):
    "Returns the mode and the standard deviation from a categorical variable given its (sorted) histogram"
    imode = np.argmax(counts)

    imin, imax = 0, counts.size-1
    frac = counts[imode] / counts.sum()
    isdev = 0
    while not frac > 0.68:
        isdev += 1
        ipos, fpos = max(imin, imode-isdev), min(imax, imode+isdev)
        frac = counts[ipos:fpos].sum() / counts.sum()
    return imode, isdev-1


def parse_types_range(types_range, barredness=""):
    type_min, type_max = types_range.agg((min, max)).values

    itype, ftype = type_min[0], type_max[0]
    if itype == ftype:
        return f"{itype}{barredness}{type_min[1:]}~--~{type_max[1:]}"
    else:
        if barredness != "":
            return f"{itype}{barredness if itype != 'E' else ''}{type_min[1:]}~--~{ftype}{barredness if ftype != 'E' else ''}{type_max[1:]}"


def parse_table_st(table_st, mu_name="mean", sigma_name="std", precision=2):
    table = table_st.round(precision)
    return table.apply(lambda r: fr"${r[mu_name]:.2f}\pm{r[sigma_name]:.2f}$")


def ellipse(fac, radius, x, y, xc, yc, pa, ellipticity, image_scale):
    a = fac * radius / image_scale
    f = 1 - np.sqrt(1 - ellipticity**2)
    b = a * (1 - f)

    ft = (x-xc)*np.cos(pa) + (y-yc)*np.sin(pa)
    st = (x-xc)*np.sin(pa) - (y-yc)*np.cos(pa)
    return (ft / a)**2 + (st / b)**2 - 1


def find_distance(guess, radius, x, y, xc, yc, pa, ellipticity, image_scale):
    f = least_squares(ellipse, x0=guess, args=(radius, x, y, xc, yc, pa, ellipticity, image_scale), method="lm", xtol=0.1).x
    return f
 