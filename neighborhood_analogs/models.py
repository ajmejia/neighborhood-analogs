
import numpy as np


def Sigma_mass(r, Rd=2.5, Md=6e10):
    """Assumed shape of the stellar mass distribution in the thin disk (Nesti & Salucci 2013)"""
    return Md / (2 * np.pi * Rd**2) * np.exp(-r / Rd)
