
import socket
import os


# letter v1 == v1.2
CURRENT_RUN_VERSION = "v1.2"
# CURRENT_RUN_VERSION = "test"

ROOT_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

HOSTNAME = socket.gethostname()
if HOSTNAME == "orion":
    CALIFA_DATA_PATH = f"{ROOT_PATH}/_data/cache"
    CALIFA_CATALOGUE_PATH = f"{ROOT_PATH}/_data/catalogs/get_proc_elines_CALIFA.all_good.csv"
    CALIFA_MORPHOLOGY_PATH = f"{ROOT_PATH}/_data/catalogs/CALIFA_3_joint_classnum_new_update.csv"
    CALIFA_AGN_PATH = f"{ROOT_PATH}/_data/catalogs/Lacerda_2019_ion_agn_flags.csv"
    CALIFA_VCOR_PATH = f"{ROOT_PATH}/_data/catalogs/get_mag_cubes_v2.2_Vmax.csv"
    MASTAR_DATA_PATH = f"{ROOT_PATH}/_data/mastar-catalogue.fits.gz"
elif HOSTNAME == "chiripa":
    CALIFA_DATA_PATH = "/disk-a/mejia/califa-z-distributions-data/cache/"
    CALIFA_CATALOGUE_PATH = "/disk-a/mejia/califa-z-distributions-data/catalogs/get_proc_elines_CALIFA.all_good.csv"
    CALIFA_MORPHOLOGY_PATH = "/disk-a/mejia/califa-z-distributions-data/catalogs/CALIFA_3_joint_classnum_new_update.csv"
    CALIFA_AGN_PATH = "/disk-a/mejia/califa-z-distributions-data/catalogs/Lacerda_2019_ion_agn_flags.csv"
    # CALIFA_VCOR_PATH = "/disk-a/mejia/califa-z-distributions-data/catalogs/CALIFA_basic_joint_phot.csv"
    CALIFA_VCOR_PATH = "/disk-a/mejia/califa-z-distributions-data/catalogs/get_mag_cubes_v2.2_Vmax.csv"
    MASTAR_DATA_PATH = "/disk-a/mejia/Research/UNAM/mastar-checks/notebooks/analysis/_data/mastar-catalogue.fits.gz"
CALIFA_CATALOG_URLS = [CALIFA_MORPHOLOGY_PATH, CALIFA_CATALOGUE_PATH, CALIFA_AGN_PATH, CALIFA_VCOR_PATH]

# CALIFA constants
CALIFA_SCALE = 1.0  # arcsec/pix
# CALIFA_PSF = 2.5 / 2.355 / CALIFA_SCALE # pix
CALIFA_PSF = 2.5
CALIFA_ALPHA = 1.06
CALIFA_INST = 6.0
HUBBLE_CLASSIFICATION = [
    "E0",
    "E1",
    "E2",
    "E3",
    "E4",
    "E5",
    "E6",
    "E7",
    "S0",
    "S0a",
    "Sa",
    "Sab",
    "Sb",
    "Sbc",
    "Sc",
    "Scd",
    "Sd",
    "Sdm",
    "Sm",
    "I",
    "X"
]
SPS_CUBE_FMT = "{}.Pipe3D.cube.fits.gz"
OBS_CUBE_FMT = "{}.V500.rscube.fits.gz"
MOD_CUBE_FMT = "SSP_mod.{}.cube.fits.gz"
SFRG_MAP_FMT = "{}.p_e.Sigma_SFR.fits.gz"
SFRS_MAP_FMT = "{}.p_e.Sigma_SFR_ssp.fits.gz"
SMAS_MAP_FMT = "{}.p_e.Sigma_Mass.fits.gz"
MLR_MAP_FMT = "{}.p_e.ML.fits.gz"

VEL_IDX = 13
SIGMA_IDX = 15
AV_IDX = 11
SPS_MAP_NAMES = {
    'age_L [log/yr]': 5,
    'age_M [log/yr]': 6,
    'Z_L [log/Zsun]': 8,
    'Z_M [log/Zsun]': 9,
    'Av [mag]': 11,
    'vel [km/s]': 13,
    'LOSVD [km/s]': 15,
    '<M/L> [log/Msun/Lsun]': 17,
    'Sigma_mass_px [log/Msun/spaxel^2]': 18,
    'Sigma_mass_px_nd [log/Msun/spaxel^2]': 19,
}
ADD_MAP_NAMES = [
    "Sigma_sfr_ha [log/Msun/yr/pc^2]",
    "Sigma_sfr_s [log/Msun/yr/pc^2]",
    "Sigma_mass [log/Msun/pc^2]",
    # "M/L [Msun/Lsun]"
]
SNA_MAP_NAMES = {name: i for i, name in enumerate(
    list(SPS_MAP_NAMES.keys())+ADD_MAP_NAMES)}
SNA_MAP_IXLOG = [i for name, i in SNA_MAP_NAMES.items() if "log" in name]

# SNA finder constants
NORM_WINDOW = 5450, 5550
RGB_WAVELENGTHS = 4550, 5500, 6500
RGB_HW = 50
FITTING_WINDOW = (3800, 7000)
CHISQ_WINDOW = (0.7, 1.3)

# choose 3 beauties:
# NAMES = ["NGC5947", "IC1078", "NGC0477"]
NAMES = ["NGC5947", "NGC5000", "NGC0309"]
# UGC04262, UGC12250, NGC0036, NGC0257, NGC0309, NGC0776
# NGC5000, UGC02311, UGC08781
COLORS = ["#017B92", "#FF6BB5", "#B36FF6"]

FIELD_NAMES = [
    'type',
    'mass_star',
    'sfr_star',
    're_kpc',
    'age_L',
    'age_M',
    'Z_L',
    'Z_M',
    'Av',
    # 'M/L',
    '<M/L>',
    'Sigma_mass',
    'Sigma_sfr_s',
    'r_sna',
    'vel',
    'LOSVD',
    'vel_to_sig'
]
FIELD_LATEX = [
    r"Type",
    r"$\log{M_\star/\textrm{M}_\odot}$",
    r"$\log{\psi_\star}$",
    r"$r_\mathrm{eff}$",
    r"$\left<\log{t/\textrm{yr}}\right>_{L_\star}$",
    r"$\left<\log{t/\textrm{yr}}\right>_{M_\star}$",
    r"$\left<[Z/\textrm{Z}_{\odot}]\right>_{L_\star}$",
    r"$\left<[Z/\textrm{Z}_{\odot}]\right>_{M_\star}$",
    r"$A_V$",
    # r"$\log{M_\star/L_\star}$",
    r"$\left<\log{M_\star/L_\star}\right>$",
    r"$\log{\Sigma_{M_\star}}$",
    r"$\log{\Sigma_{\psi_\star}}$",
    r"$r_\mathrm{SNA}/r_\mathrm{eff}$",
    r"$v_\star$",
    r"$\sigma_\star$",
    r"$v_\star/\sigma_\star$"
]
UNITS = [
    r"",
    r"dex",
    r"$\textrm{M}_\odot\,\textrm{yr}^{-1}$",
    r"kpc",
    r"dex",
    r"dex",
    r"dex",
    r"dex",
    r"mag",
    # r"$\textrm{M}_\odot\,{\textrm{L}_\odot}^{-1}$",
    r"$\textrm{M}_\odot\,{\textrm{L}_\odot}^{-1}$",
    r"$\textrm{M}_\odot\,\textrm{pc}^{-2}$",
    r"$\textrm{M}_\odot\,\textrm{yr}^{-1}\,\textrm{pc}^{-2}$",
    r"",
    r"$\textrm{km}\,s^{-1}$",
    r"$\textrm{km}\,s^{-1}$",
    r""
]
NAMES_MAP = dict(zip(FIELD_NAMES, FIELD_LATEX))
UNITS_MAP = dict(zip(FIELD_NAMES, UNITS))

RE_MW = 1.68 * 2.5
R_SUN = 8.2 / RE_MW
