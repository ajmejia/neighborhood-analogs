
import numpy as np
from scipy.stats import gaussian_kde
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
from astropy.visualization import hist

def projected_circle(position, pa, ellipticity, radius, image_scale, **kwargs):
    """Return an Ellipse object simulating a projected circle centered at the given position

    Parameters
    ----------
    position: float tuple
        The (x,y) coordinates where to center the projected circle.
    pa: float
        The position angle in degrees.
    ellipticity: float
        The degree of flattening.
    radius: float
        The radius of the circle in arcsec.
    image_scale: float
        The scale of the image in arcsec/pixels.
    **kwargs:
        Any matplotlib styling parameter (e.g., edgecolor, lw, etc.).

    Returns
    -------
        A matplotlib.patches.Ellipse object.
    """
    kwargs_ = dict(fc="none", ec="magenta", lw=1)
    kwargs_.update(kwargs)

    a = radius / image_scale

    f = 1 - np.sqrt(1 - ellipticity**2)
    b = a * (1 - f)

    return Ellipse(position, 2*a, 2*b, pa, **kwargs_)


def sna_hist(name, data_sna, data_mw, ax=None, show_sna_sigma=False, units=None, ex_names=None, ex_colors=None, ex_type="line"):
    """Returns axis with the corresponding histogram and PDF plotted, including the MW reference value"""
    values = data_sna.loc[:, name]
    mu_mw = data_mw.loc["mean", name]
    sg_mw = data_mw.loc["std", name]
    mu_sna, sg_sna = np.nanmedian(values), np.nanstd(values)

    pdf_sna = gaussian_kde(values.dropna(), bw_method="scott")
    domain = np.floor(values.min()), np.ceil(values.max())
    x_support = np.linspace(*domain, 1000)
    y_sna = pdf_sna(x_support)

    if ax is None:
        _, ax = plt.subplots(figsize=(5, 5))

    ax.set_xlim(*domain)
    y0, _ = ax.set_ylim(0, 1.05)
    if units is not None and units != "":
        ax.set_xlabel(f"{name} [{units}]")
    else:
        ax.set_xlabel(f"{name}")
    if ax.get_subplotspec().is_first_col():
        ax.set_ylabel(r"PDF/PDF$_\mathrm{mode}$")
    xx, xe, p = hist(values, ax=ax, bins="freedman", density=True,
                     range=domain, color="0.7", hatch="/////", align="mid")
    for item in p:
        item.set_height(item.get_height()/xx.max())
    ax.fill_between(x_support, y_sna/y_sna.max(), where=(mu_mw-sg_mw <= x_support)
                    & (x_support <= mu_mw+sg_mw), lw=0, color="darkorange", alpha=0.5, zorder=2)
    if show_sna_sigma:
        ax.fill_between(x_support, y_sna/y_sna.max(), where=(mu_sna-sg_sna <= x_support)
                        & (x_support <= mu_sna+sg_sna), lw=0, color="0.7", alpha=0.5, zorder=2)

    ax.vlines(mu_sna, ymin=0, ymax=pdf_sna(mu_sna) /
              y_sna.max(), ls="--", color="0.5", zorder=3)
    ax.vlines(mu_mw, ymin=0, ymax=pdf_sna(mu_mw) / y_sna.max(),
              ls="--", color="darkorange", zorder=3)

    ax.plot(x_support, y_sna/y_sna.max(), lw=2, color="0.5", zorder=4)
    if ex_names is not None and ex_colors is not None:
        if ex_type == "dot":
            ax.scatter(values.loc[ex_names], pdf_sna(
                values.loc[ex_names])/y_sna.max(), lw=0, s=70, c=ex_colors, zorder=5)
        elif ex_type == "line":
            ax.vlines(values.loc[ex_names], ymin=y0, ymax=pdf_sna(
                values.loc[ex_names])/y_sna.max(), ls="-.", colors=ex_colors, zorder=5)
        else:
            raise NotImplementedError(f"{ex_type = } is not implemented.")

    return ax
